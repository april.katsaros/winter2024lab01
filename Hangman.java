import java.util.Scanner;
public class Hangman{

	public static int isLetterInWord(String word, char c) {
	for (int i = 0; i < word.length(); i++){
		
		if (word.charAt(i) == c) {
			return i;
		}
		
	}
	   return -1;
	}
	public static char toUpperCase(char c) {
		return Character.toUpperCase(c);
	}
	public static void printWork(String word, boolean[] letters) {
		char letter0Result = '_';
		char letter1Result = '_';
		char letter2Result = '_';
		char letter3Result = '_';
			
		if (letters[0]){
			letter0Result = word.charAt(0);
			
		}
		if (letters[1]){
			letter1Result = word.charAt(1);
			
		}
		if (letters[2]){
			letter2Result = word.charAt(2);
			
		}

		if (letters[3]){
			letter3Result = word.charAt(3);
			
		}
		String result = letter0Result + " " + letter1Result + " " + letter2Result + " " + letter3Result;
		System.out.println("Your result is " + result);
	}
	public static  void runGame(String word){
		boolean[] letters = {false, false, false, false};
		int incorrectGuesses = 0;
		final int FIRST_POSITION_LETTER = 0;
		final int SECOND_POSITION_LETTER = 1;
		final int THIRD_POSITION_LETTER = 2;
		final int FOURTH_POSITION_LETTER = 3;
		final int INCORRECT_LETTER = -1;
		
		while(incorrectGuesses < 6 && !(letters[0] && letters[1] && letters[2] && letters[3])){

			Scanner reader = new Scanner(System.in);
			System.out.println("Guess a letter");
			char guess = reader.nextLine().charAt(0);
			guess = toUpperCase(guess);
			
			if(isLetterInWord(word, guess) == FIRST_POSITION_LETTER){
				letters[0] = true;
			}
			if(isLetterInWord(word, guess) == SECOND_POSITION_LETTER){
				letters[1] = true;
			}
			if(isLetterInWord(word, guess) == THIRD_POSITION_LETTER){
				letters[2] = true;
			}
			if(isLetterInWord(word, guess) == FOURTH_POSITION_LETTER){
				letters[3] = true;
			}
            if(isLetterInWord(word, guess) == INCORRECT_LETTER){
				incorrectGuesses++;
			}
			printWork(word, letters);
		}
		boolean lose = incorrectGuesses == 6;
		boolean win = letters[0] && letters[1] && letters[2] && letters[3];
		
		if (lose){
		System.out.println("Oops! You ran out of guesses! Better luck next time :)" );
		}
		if (win){
		System.out.println("Congrats! You guessed the word :)");
		}
	}
	public static void main(String[] args){
		Scanner reader = new Scanner (System.in);
		//Get user input
		System.out.println("Enter a 4-letter word:");
		String word = reader.next();
		//Convert to upper case
		word = word.toUpperCase();
		
		//Start hangman game
	    runGame(word);	
	}
}